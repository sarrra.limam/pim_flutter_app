# MediaSI

A working mediaSI  written in Flutter using MongoDB with a desktop version
the concept of the application is sharing , uploading and downloading informations and
some news in the world.
The role of the project MediaSI is to catch the fake news and minimize it to observe just 
the real news and informations with sources and links talking about those informations and 
explore them.

# NOTE

This repository is still under development and I will continue to add more features to it.

# Features

- Post news by adding description and uploading photo from gallery organized by category
- Delete posts
- Comment posts
- View and delete comments a post
- Like / Dislike posts
- View all likes on a post
- Home screen
- Profile Screen with possibility of editing

# Screenshots

![image](./assets/screenshots/signIn.png)  ![image](./assets/screenshots/signUp.png) 
![image](./assets/screenshots/profile.png)  ![image](./assets/screenshots/News.png) 
![image](./assets/screenshots/Myposts.png)  ![image](./assets/screenshots/message.png) 
![image](./assets/screenshots/menu.png)     ![image](./assets/screenshots/listComments.png) 
![image](./assets/screenshots/editProfile.png)  ![image](./assets/screenshots/categories.png) 
![image](./assets/screenshots/DetailNews.png)  ![image](./assets/screenshots/addNews.png) 
![image](./assets/screenshots/addComment.png) 


# Getting started

1. Setup Flutter
2. Setup MongoDB

3. Clone the repo

    $ git clone https://gitlab.com/sarrra.limam/pim_flutter_app.git
    $ cd pim_flutter_app/

# How to Contribute

    1. Fork the project
    2. Create your feature branch (git checkout -b my-new-feature)
    3. Make required changes and commit (git commit -am 'Add some feature')
    4. Push to the branch (git push origin my-new-feature)
    5. Create new Pull Request


# What's Next?

- Edit profile
- Chat with any user
- vote news

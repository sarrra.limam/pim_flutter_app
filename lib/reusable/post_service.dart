
import 'package:flutter/foundation.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pim_flutter_app/model/Post.dart';

List<Post> parsePost(String responseBody){
  var list = json.decode(responseBody) as List<dynamic>;
  List<Post> posts = list.map((model) => Post.fromJson(model)).toList();
  return posts ;
}

Future<List<Post>> getPosts() async{
  //final response = await http.get('http://localhost:3000/api/posts');
  final response = await http.get(
    Uri.http("10.0.2.2:5000", "api/posts/"),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
  );

  if(response.statusCode == 200){
    return compute(parsePost, response.body);

  }
  else{
    throw Exception('Request API Error');
  }

}
Future<List<Post>> getPostBYCategory(String cat) async{
  //final response = await http.get('http://localhost:3000/api/posts');
  final response = await http.get(
    Uri.http("10.0.2.2:5000", "api/posts/getPostBYCategory"),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Category': cat,
    },
  );

  if(response.statusCode == 200){
    return compute(parsePost, response.body);

  }
  else{
    throw Exception('Request API Error');
  }


}
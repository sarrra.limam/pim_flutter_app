import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pim_flutter_app/home.dart';
import 'package:pim_flutter_app/model/LoginUser.dart';

import 'package:shared_preferences/shared_preferences.dart';

class AddNews extends StatefulWidget {
  @override
  _AddNewsPage createState() => _AddNewsPage();
}

class _AddNewsPage extends State<AddNews> {


  String title;
  String description;
  String state = "";
  var file;
  File _image;
  String dropdownValue = 'Top stories';
  Future getImage() async{
    final image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = image;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
          children: <Widget>[

      SizedBox(
        height: 20,
      ),
            _image == null ? Text("No Image") : Image.file(_image),

            IconButton(
              alignment: Alignment.topCenter,
              icon: Icon(Icons.add_a_photo,color: Color(0xffff0000),),
              onPressed: () async{
               getImage();
              },
            ),
      Padding(
        padding: const EdgeInsets.all(20.0),
        child: Row(
          children: <Widget>[

            Expanded(
                child: Container(
                    margin: EdgeInsets.only(right: 20, left: 10),
                    child: TextField(
                      decoration: InputDecoration(hintText: 'Title'),
                      onChanged: (value){
                        title = value;
                      },
                    )))
          ],
        ),
      ),
      SizedBox(
        height: 20,
      ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Row(
                children: <Widget>[

                  Expanded(
                      child: Container(
                          margin: EdgeInsets.only(right: 20, left: 10),
                          child: TextField(
                            keyboardType: TextInputType.multiline,
                            maxLines: 5,
                            decoration: InputDecoration(hintText: 'Description'),
                            onChanged: (value){
                              description = value;
                            },
                          )))
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Row(
                children: <Widget>[

                  Expanded(
                      child: Container(
                          margin: EdgeInsets.only(right: 20, left: 10),
                          child: DropdownButton<String>(
                            value: dropdownValue,
                            icon: const Icon(Icons.arrow_downward),
                            iconSize: 24,
                            elevation: 16,
                            style: const TextStyle(color: Colors.red),
                            underline: Container(
                              height: 2,
                              color: Colors.red,
                            ),
                            onChanged: (String newValue) {
                              setState(() {
                                dropdownValue = newValue;
                              });
                            },
                            items:<String>['Top stories','Tunisia','World','Business','Sports','Tech','Education','Entertainment','Music','Lifestyle','Health and Fitness','Fashion','Art and Culture','Travel','Books']
                                .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),

                      )))
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),

            OutlineButton(
              textColor: Colors.white,
              padding: EdgeInsets.all(0.0),
              shape: StadiumBorder(),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25.0),
                  gradient: LinearGradient(
                    colors: <Color>[
                      Color(0xffff0000),
                      Color(0xffff0000),
                    ],
                  ),
                ),
                child: Text(
                  'Add News',
                  style: TextStyle(fontSize: 15.0),
                ),
                padding: EdgeInsets.symmetric(horizontal: 120.0, vertical: 20.0),
              ),
              onPressed: () async{
                var res = await uploadImage(_image.path,description,title,dropdownValue);
                setState(() {
                  state = res;

                });
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => Home()));

              },
            ),


      ],

      )
    );
  }


  Future<String> uploadImage(filename,description,title,category) async {
    var url = "10.0.2.2:5000/api/posts/add";
    SharedPreferences prefs1 = await SharedPreferences.getInstance();
    //Return int
    String user = prefs1.getString("id");
    var request = http.MultipartRequest('POST', Uri.http("10.0.2.2:5000", "api/posts/add"));
    request.files.add(await http.MultipartFile.fromPath('file', filename));
    request.fields['text'] = description;
    request.fields['title'] = title;
    request.fields['category'] = category;
    request.fields['userid'] = user;
    var res = await request.send();
    return res.reasonPhrase;
  }
}
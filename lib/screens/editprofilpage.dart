import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

const url = 'http://bunnypay.com';

const location = 'Tunis, Tunisia';
var parentId;
String parentFirstName;
String parentLastName;
String parentPhone;
String parentEmail;

final textController_1 = TextEditingController()..text=parentPhone;

TextEditingController fr = TextEditingController()..text = parentFirstName;
TextEditingController ls = TextEditingController()..text = parentLastName;
TextEditingController em = TextEditingController()..text = parentEmail;
TextEditingController pho = TextEditingController()..text = parentPhone;

GetParent() async{



  SharedPreferences prefs1 = await SharedPreferences.getInstance();
  //Return int
  String parentId = prefs1.getString("id");
  var url = Uri.http("10.0.2.2:5000", "asapi/profile/user/"+parentId);
  final response = await http.get(
    url,

  );
  print(response.body);


  var parse = jsonDecode(response.body);

  parentFirstName = parse["firstname"];
  parentLastName = parse["lastname"];
  parentPhone = parse["phone"];
  parentEmail = parse["email"];


  print(parentEmail);

  print(parentFirstName);


}

class SettingsUI extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Setting UI",
      home: EditProfilPage(),
    );
  }
}

class EditProfilPage extends StatefulWidget {
  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilPage> {


  @override
  void initState(){
    GetParent();
  }


  void _showDialog(BuildContext context, {String title, String msg}) {
    final dialog = AlertDialog(
      title: Text(title),
      content: Text(msg),
      actions: <Widget>[
        RaisedButton(
          color: Color(0xff511f52),
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text(
            'Close',
            style: TextStyle(
              color: Colors.white,
            ),
          ),
        ),
      ],
    );
    showDialog(context: context, builder: (x) => dialog);
  }


  bool showPassword = false;
  String title;
  String description;
  String state = "";
  String dropdownValue = 'Top stories';
  var file;
  File _image;

  Future getImage() async {
    final image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = image;
    });
  }


  var firstname, lastname, phone, email, oldPassword, newPassword,
      ConfirmpasswordP;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(left: 16, top: 25, right: 16),
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: ListView(
            children: [
              Text(
                "Edit Profile",
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.w500),
              ),
              SizedBox(
                height: 15,
              ),
              Center(
                child: Stack(
                  children: [
                    Container(
                      width: 130,
                      height: 130,
                      decoration: BoxDecoration(
                          border: Border.all(
                              width: 4,
                              color: Theme
                                  .of(context)
                                  .scaffoldBackgroundColor),
                          boxShadow: [
                            BoxShadow(
                                spreadRadius: 2,
                                blurRadius: 10,
                                color: Colors.red.withOpacity(0.1),
                                offset: Offset(0, 10))
                          ],
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(
                                "https://images.pexels.com/photos/3307758/pexels-photo-3307758.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=250",
                              ))),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    _image == null ? Text("No Image") : Image.file(_image),

                    Positioned(
                        bottom: 0,
                        right: 0,
                        child: Container(
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                              width: 4,
                              color: Theme
                                  .of(context)
                                  .scaffoldBackgroundColor,
                            ),
                            color: Colors.red,
                          ),
                          child: IconButton(
                            alignment: Alignment.topCenter,
                            icon: Icon(Icons.edit,
                              color: Colors.white,
                            ),
                            onPressed: () async {
                              getImage();
                            },
                          ),
                        )),
                  ],
                ),
              ),
              SizedBox(
                height: 35,
              ),
              buildTextField("First Name", fr.text, false),
              buildTextField("Last Name",ls.text, false),
              buildTextField("E-mail", em.text, true),
              buildTextField("Phone", pho.text, false),
              SizedBox(
                height: 35,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  OutlineButton(
                    padding: EdgeInsets.symmetric(horizontal: 50),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    onPressed: () {
                      Navigator.pushNamed(context, 'profilepage');
                    },
                    child: Text("CANCEL",
                        style: TextStyle(
                            fontSize: 14,
                            letterSpacing: 2.2,
                            color: Colors.red)),
                  ),
                  RaisedButton(
                    onPressed: () async {
                      var res = await uploadImage(
                          _image.path, description, title, dropdownValue);
                      setState(() {
                        state = res;
                      });

                      Navigator.pushNamed(context, 'profilepage');
                    },

                    color: Colors.red,
                    padding: EdgeInsets.symmetric(horizontal: 50),
                    elevation: 2,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    child: Text(
                      "SAVE",
                      style: TextStyle(
                          fontSize: 14,
                          letterSpacing: 2.2,
                          color: Colors.white),
                    ),


                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget buildTextField(String labelText, String placeholder,
      bool isPasswordTextField) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 35.0),
      child: TextField(
        obscureText: isPasswordTextField ? showPassword : false,
        decoration: InputDecoration(

            contentPadding: EdgeInsets.only(bottom: 3),
            labelText: labelText,
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: placeholder,
            hintStyle: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            )),
      ),
    );
  }

/*  Widget buildTextField(
      String labelText, String placeholder, bool isPasswordTextField) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 35.0),
      child: TextField(
        obscureText: isPasswordTextField ? showPassword : false,
        decoration: InputDecoration(
            suffixIcon: isPasswordTextField
                ? IconButton(
              onPressed: () {
                setState(() {
                  showPassword = !showPassword;
                });
              },
              icon: Icon(
                Icons.remove_red_eye,
                color: Colors.grey,
              ),
            )
                : null,
            contentPadding: EdgeInsets.only(bottom: 3),
            labelText: labelText,
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: placeholder,
            hintStyle: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            )),
      ),
    );
  }*/
  Future<String> uploadImage(filename, description, title, category) async {
    var url = "10.0.2.2:5000/api/posts/add";
    SharedPreferences prefs1 = await SharedPreferences.getInstance();
    //Return int
    String user = prefs1.getString("id");
    var request = http.MultipartRequest(
        'POST', Uri.http("10.0.2.2:5000", "api/posts/add"));
    request.files.add(await http.MultipartFile.fromPath('file', filename));
    request.fields['full name'] = description;
    request.fields['email'] = title;
    request.fields['password'] = category;
    request.fields['location'] = user;
    var res = await request.send();
    return res.reasonPhrase;
  }
}
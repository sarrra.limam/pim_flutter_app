import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:pim_flutter_app/model/News.dart';
import 'package:pim_flutter_app/reusable/custom_cards.dart';
import 'package:pim_flutter_app/model/categories_model.dart';
import 'package:pim_flutter_app/model/Post.dart';
import 'package:pim_flutter_app/reusable/post_service.dart';
import 'dart:async';

import 'detailsnews.dart';

class NewsByCat extends StatefulWidget {
  final String catName;
  NewsByCat(this.catName);
  @override
  _NewsByCatState createState() => _NewsByCatState();
}

class _NewsByCatState extends State<NewsByCat>

{
  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("News"),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[
                    Color(0xd3fa022f),
                    Color(0xffe56464)
                  ])
          ),
        ),
        backgroundColor: Colors.red,
      ),

      body: Container(
        child: FutureBuilder(
            future: widget.catName == "" ? getPosts() : getPostBYCategory(widget.catName),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return InkWell(
                      child: Padding(
                        padding: EdgeInsets.only(top: 15),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              height: 203,
                              width: MediaQuery
                                  .of(context)
                                  .size
                                  .width,

                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(
                                  color: Color(0xff707070),
                                  width: 1,
                                ),
                                image: DecorationImage(
                                    image: NetworkImage(
                                        "http://192.168.68.1:5000/uploads/" +
                                            snapshot.data[index].photo),
                                    fit: BoxFit.fill),

                              ),
                              child: Align(
                                alignment: Alignment.bottomCenter,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Container(
                                      width: MediaQuery
                                          .of(context)
                                          .size
                                          .width,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(11),
                                        color: Colors.black.withOpacity(0.33),
                                      ),
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 10),
                                      child: Center(
                                        child: Text(
                                          snapshot.data[index].text,
                                          style: TextStyle(
                                              fontFamily: "Avenir",
                                              fontSize: 16,
                                              color: Colors.white),
                                          maxLines: 3,
                                          overflow: TextOverflow.fade,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),

                            SizedBox(
                              height: 10,
                            ),
                            Text(snapshot.data[index].date,
                                style: TextStyle(
                                    fontFamily: "Times",
                                    fontSize: 13,
                                    color: Color(0xff8a8989))),
                            SizedBox(
                              height: 7,
                            ),
                            Text(snapshot.data[index].title,
                                style: TextStyle(
                                    fontFamily: "League",
                                    fontSize: 23,
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                      onTap: () {
                        Navigator.push(context,
                            new MaterialPageRoute(builder: (context) =>
                                DetailsNews(snapshot.data[index])));
                      },
                    );
                  },
                  padding: EdgeInsets.symmetric(horizontal: 25),
                );
              }
              else if (snapshot.hasError) {
                return Container(
                  child: Center(child: Text('Not Found Data')),
                );
              } else {
                return Container(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              }
            }
        ),
      ),

    );
  }
}

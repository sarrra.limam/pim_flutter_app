import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:pim_flutter_app/model/News.dart';
import 'package:pim_flutter_app/reusable/custom_cards.dart';
import 'package:pim_flutter_app/model/categories_model.dart';
import 'package:pim_flutter_app/model/Post.dart';
import 'package:pim_flutter_app/reusable/post_service.dart';
import 'dart:async';

import 'detailsnews.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
   with SingleTickerProviderStateMixin {
  /*
  ScrollController _scrollController;
  TabController _tabController;
  int currentIndex = 0;

  @override
  void initState() {

    super.initState();
    _scrollController = ScrollController();
    _tabController = TabController(length: categories.length, vsync: this);
    _tabController.addListener(_smoothScrollToTop);
  }

  _smoothScrollToTop() {
    if (_scrollController.hasClients)
      _scrollController.animateTo(
        0,
        duration: Duration(microseconds: 300),
        curve: Curves.ease,
      );
  }

  void changePage(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollView(
        controller: _scrollController,
        headerSliverBuilder: (context, value) {
          return [
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(25, 10, 25, 25),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Top News Updates",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontFamily: "Times",
                      fontSize: 34,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: Container(
                padding: EdgeInsets.only(left: 25),
                alignment: Alignment.centerLeft,
                child: TabBar(
                    labelPadding: EdgeInsets.only(right: 15),
                    indicatorSize: TabBarIndicatorSize.label,
                    controller: _tabController,
                    isScrollable: true,
                    indicator: UnderlineTabIndicator(),
                    labelColor: Colors.black,
                    labelStyle: TextStyle(
                        fontFamily: "Avenir",
                        fontSize: 19,
                        fontWeight: FontWeight.bold),
                    unselectedLabelColor: Colors.black45,
                    unselectedLabelStyle: TextStyle(
                        fontFamily: "Avenir",
                        fontSize: 18,
                        fontWeight: FontWeight.normal),
                    tabs: List.generate(categories.length,
                            (index) => Text(categories[index].name))),
              ),
            ),
          ];
        },
        body: Container(
          child: TabBarView(controller: _tabController,
              children: List.generate(categories.length, (index) {
                return ListView.builder(
                  itemCount: 10, itemBuilder: (context, index) {
                  return HomePageCard();
                },
                  padding: EdgeInsets.symmetric(horizontal: 25),
                );
              })),
        ));
  }

}
*/


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(

        child: FutureBuilder(

            future: getPosts(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(

                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {

                    return InkWell(
                      child: Padding(
                        padding: EdgeInsets.only(top: 15),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [

                            Container(
                              height: 203,
                              width: MediaQuery
                                  .of(context)
                                  .size
                                  .width,

                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(
                                  color: Color(0xff707070),
                                  width: 1,
                                ),
                                image: DecorationImage(
                                    image: NetworkImage(
                                        "http://192.168.68.1:5000/uploads/" +
                                            snapshot.data[index].photo),
                                    fit: BoxFit.fill),

                              ),
                              child: Align(
                                alignment: Alignment.bottomCenter,
                                child: Column(

                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [

                                    Container(
                                      width: MediaQuery
                                          .of(context)
                                          .size
                                          .width,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(11),
                                        color: Colors.black.withOpacity(0.33),
                                      ),
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 10),
                                      child: Center(
                                        child: Text(
                                          snapshot.data[index].text,
                                          style: TextStyle(
                                              fontFamily: "Avenir",
                                              fontSize: 16,
                                              color: Colors.white),
                                          maxLines: 3,
                                          overflow: TextOverflow.fade,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),

                            SizedBox(
                              height: 10,
                            ),
                            Text(snapshot.data[index].date,
                                style: TextStyle(
                                    fontFamily: "Times",
                                    fontSize: 13,
                                    color: Color(0xff8a8989))),
                            SizedBox(
                              height: 7,
                            ),
                            Text(snapshot.data[index].title,
                                style: TextStyle(
                                    fontFamily: "League",
                                    fontSize: 23,
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                      onTap: () {
                        Navigator.push(context,
                            new MaterialPageRoute(builder: (context) =>
                                DetailsNews(snapshot.data[index])));
                      },
                    );
                  },
                  padding: EdgeInsets.symmetric(horizontal: 25),
                );
              }
              else if (snapshot.hasError) {
                return Container(
                  child: Center(child: Text('Not Found Data')),
                );
              } else {
                return Container(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              }
            }
        ),
      ),

      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.red,
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50)
          ),
          child: Icon(Icons.add),

        ),
        onPressed: () {
          Navigator.pushNamed(context, 'AddNews');
          // Navigator.of(context).push(MaterialPageRoute(builder: (context) => editprofilpage()));
          // Navigator.pushNamed(context, 'editprofilpage');
        },
      ),

    );
  }
}

import 'package:flutter/material.dart';
import 'package:pim_flutter_app/model/Comments.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;



/// This Widget is the main application widget.
class ListComments extends StatefulWidget {
  static const String _title = 'Food App Sample';
  final String postId;
  ListComments(this.postId);

  @override
  _ListCommentsState createState() => _ListCommentsState();
}

class _ListCommentsState extends State<ListComments> {
  List data;


  GetComments() async {
    final response = await http.post(
      Uri.http("10.0.2.2:5000", "api/posts/getCommentsByPost"),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'postId':widget.postId,

      }),
    );
    //print(response.body);

    this.setState(() {
      data = json.decode(response.body);
    });
    print(data);
  }
  @override
  void initState() {

    super.initState();
     GetComments();

  }

  deletecomment(commnid) async{


    final response = await http.post(
      Uri.http("10.0.2.2:5000", "api/posts/deleteComm"),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'publicationId':widget.postId,
        'comId':commnid


      }),
    );
    print(widget.postId);
    print(commnid);
    print(response.body);
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(

          title: Text("Comments"),

          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: <Color>[
                      Color(0xd3fa022f),
                      Color(0xffe56464)
                    ])
            ),
          ),
          backgroundColor: Colors.red,
        ),
        body: FutureBuilder(

    builder: (BuildContext context, AsyncSnapshot snapshot){
    if(data!= null){
       return new ListView.builder(
          itemCount: data == null ? 0 : data.length,
          itemBuilder: (BuildContext context, int index){
            return Container(

                child: Padding(
                  padding: EdgeInsets.all(12),
                  child:  Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5), // if you need this
                      side: BorderSide(
                        color: Colors.grey.withOpacity(.2),
                        width: 1,
                      ),
                    ),
                    child: Container(
                      width: 400,
                      height: 170,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.fromLTRB(8.0, 13.0, 8.0, 0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  height: 50.0,
                                  width: 60.0,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(15.0),
                                    child:Container(
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(15.0),
                                        image: DecorationImage(
                                            image: NetworkImage(
                                               data[index]["avatar"]),
                                            fit: BoxFit.fill),
                                        ),
                                    )
                                  ),
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(0, 6.0, 0, 0),
                                      child: Text(
                                        data[index]["username"],
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 18,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    ),

                                  ],
                                ),
                              ],
                            ),
                          ),
                          Divider(
                            color: Colors.black,
                            thickness: .2,
                            indent: 8,
                            endIndent: 8,
                          ),
                          Container(
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(8.0, 0, 0, 0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    data[index]["text"],
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),

                                  Text(
                                    data[index]["date"],
                                    style: TextStyle(color: Colors.grey[500],fontSize:  10),
                                  ),

                                ],
                              ),
                            ),
                          ),
                          Divider(
                            color: Colors.black,
                            thickness: .2,
                            indent: 8,
                            endIndent: 8,
                          ),
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    'Delivered',
                                    style: TextStyle(color: Colors.white),
                                  ),

                                  /* ButtonBar(
                          alignment: MainAxisAlignment.end,
                          children: [
                            FlatButton(
                              child: Icon(Icons.delete, color: Colors.red),
                              //Text('delete this post'),
                              textColor: Colors.red,
                              onPressed: () {
                                //deletepost(data[index]["_id"]);
                              },
                            ),

                          ],
                        )*/
                                  GestureDetector(
                                    onTap: () async{ await deletecomment(data[index]["_id"]);},
                                    child: Container(
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.end,
                                        children: <Widget>[
                                          Padding(
                                            padding:
                                            const EdgeInsets.fromLTRB(0, 0, 0, 2.0),
                                            child: Icon(
                                              Icons.delete,
                                              size: 18,
                                              color: Colors.red,
                                            ),
                                          ),

                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ));



          },
        );
    }
    else if(snapshot.hasError){
    return Container(
    child: Center(child: Text('Not Found Data')),
    );
    }else{
    return Container(
    child: Center(
    child: CircularProgressIndicator(),
    ),
    );
    }
  })



      ),
    );
  }
}

/// This is the stateless widget that the main application instantiates.
class MyStatelessWidget extends StatelessWidget {
  MyStatelessWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {


    return Column(
      children: <Widget>[
        Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5), // if you need this
            side: BorderSide(
              color: Colors.grey.withOpacity(.2),
              width: 1,
            ),
          ),
          child: Container(
            width: 400,
            height: 170,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(8.0, 13.0, 8.0, 0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 50.0,
                        width: 60.0,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(15.0),
                          child: Image.network(
                            'https://onlinejpgtools.com/images/examples-onlinejpgtools/coffee-resized.jpg',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 6.0, 0, 0),
                            child: Text(
                             "Sarra",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),

                        ],
                      ),
                    ],
                  ),
                ),
                Divider(
                  color: Colors.black,
                  thickness: .2,
                  indent: 8,
                  endIndent: 8,
                ),
                Container(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(8.0, 0, 0, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Commentaire',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),

                        Text(
                          '28 Feb 2020 at 1:36 PM',
                          style: TextStyle(color: Colors.grey[700]),
                        ),

                      ],
                    ),
                  ),
                ),
                Divider(
                  color: Colors.black,
                  thickness: .2,
                  indent: 8,
                  endIndent: 8,
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Delivered',
                          style: TextStyle(color: Colors.white),
                        ),

                       /* ButtonBar(
                          alignment: MainAxisAlignment.end,
                          children: [
                            FlatButton(
                              child: Icon(Icons.delete, color: Colors.red),
                              //Text('delete this post'),
                              textColor: Colors.red,
                              onPressed: () {
                                //deletepost(data[index]["_id"]);
                              },
                            ),

                          ],
                        )*/
                       GestureDetector(
                          onTap: null,
                          child: Container(
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Padding(
                                  padding:
                                  const EdgeInsets.fromLTRB(0, 0, 0, 2.0),
                                  child: Icon(
                                    Icons.delete,
                                    size: 18,
                                    color: Colors.red,
                                  ),
                                ),

                              ],
                            ),
                          ),
                        ),


                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );

  }


}
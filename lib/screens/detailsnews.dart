import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pim_flutter_app/model/News.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:pim_flutter_app/model/Post.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'ListComments.dart';

String text;
class DetailsNews extends StatefulWidget {
  final Post post;
  DetailsNews(this.post);
  // This widget is the root of your application.

  @override
  _DetailsPage createState() => _DetailsPage();
}

class _DetailsPage extends State<DetailsNews> {
  bool liked = false;
  int nbLikes = 0 ;
  List data;
  @override
  void initState(){

    getpostliked();
    getnbLikes();


  }

  @override
  Widget build(BuildContext context) {



    Widget titleSection = Container(


        padding: const EdgeInsets.all(50),

        child: Row(
          children: [
            Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: const EdgeInsets.only(bottom: 8),
                      child: Text(widget.post.title, style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20)
                      ),
                    ),
                    Text(widget.post.text,
                        style: TextStyle(color: Colors.grey[500])
                    ),
                  ],
                )),
            //Icon(Icons.favorite, color: Colors.red),
           // Text("55")
          ],
        ));



    Widget buttonSection = Container(
      child:  Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,


          children: [
            Container(

                width: 100,


                child: ElevatedButton(
                    onPressed: () {
                      Alert(
                          context: context,
                          title: "Add Comment",
                          content: Column(
                            children: <Widget>[
                              TextField(
                                decoration: InputDecoration(
                                  labelText: 'your Comment',
                                ),
                                onChanged: (value) {
                                  text = value;
                                },
                              ),

                            ],
                          ),
                          buttons: [
                            DialogButton(
                              onPressed: (){
                                addComment(widget.post.sId, text);
                                Navigator.pop(context);
                              },
                              child: Text(
                                "Done",
                                style: TextStyle(color: Colors.white, fontSize: 20),
                              ),
                            )
                          ]).show();

                    },
                    style: ElevatedButton.styleFrom(
                      primary: Color(0xffff0000),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                    ),

                    child: Text('Comment',

                        style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 0,
                        color: Colors.white))
                )


            ),
            ElevatedButton(


              style: ElevatedButton.styleFrom(
                primary: Color(0xffff0000),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
              ),
              onPressed: () {
              },
              child: Text("Fake News",
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 0,
                      color: Colors.white)),
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Color(0xffff0000),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
              ),

              onPressed: () {
              },
              child: Text("Real News",

                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 0,
                      color: Colors.white)),
            ),
            //IconButton(icon: Icon(Icons.how_to_vote), onPressed: null),



           // _buildButtonColumn(Colors.blue, Icons.share, "SHARE")
            !liked ? IconButton(
              icon:  Icon(
                Icons.favorite_border,
                color: Colors.red,
              ),
              iconSize: 30,
              onPressed: () {
                addLike(widget.post.sId);
                liked = true;
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => super.widget));

              },
            ) :IconButton(
              icon:  Icon(
                Icons.favorite,
                color: Colors.red,
              ),
              iconSize: 30,
              onPressed: () {
              dislike(widget.post.sId);
              liked = false;
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => super.widget));

              },
            ),
            Text(nbLikes.toString())

          ]),



    );
    

    Widget descriptionSection = Container(
      padding: const EdgeInsets.all(32),

      child: OutlineButton(
        padding: EdgeInsets.symmetric(horizontal: 30),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20)),
        onPressed: () {
          //Navigator.pushNamed(context, 'liste commentaires');
          Navigator.push(context,
              new MaterialPageRoute(builder: (context) =>
                  ListComments(widget.post.sId)));
          // Navigator.of(context).push(MaterialPageRoute(builder: (context) => editprofilpage()));
          // Navigator.pushNamed(context, 'editprofilpage');
        },

        child: Text("List Comments",
            style: TextStyle(
                fontSize: 17,
                fontWeight: FontWeight.bold,
                letterSpacing: 2.2,
                color: Colors.red)),


      ),



    );






    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
          appBar: AppBar(
            title: Text("Details News"),
            flexibleSpace: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: <Color>[
                        Color(0xd3fa022f),
                        Color(0xffe56464)
                      ])
              ),
            ),
            backgroundColor: Colors.red,
          ),
          body: ListView(children: [
            Image.network("http://192.168.68.1:5000/uploads/"+widget.post.photo,
              width: 600,
              height: 220,
              fit: BoxFit.cover,
            ),
            titleSection,
            buttonSection,
            descriptionSection,
          ])),
    );
  }

  Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          padding: const EdgeInsets.only(bottom: 8),
          child: Icon(icon, color: color),
        ),

        Text(label,
            style:TextStyle( fontSize: 16, fontWeight: FontWeight.w400, color: color,
            ))
      ],
    );

  }
  getpostliked() async{

    SharedPreferences prefs1 = await SharedPreferences.getInstance();
    //Return int
    String user = prefs1.getString("id");
    final response = await http.post(
      Uri.http("10.0.2.2:5000", "api/posts/getpostlike"),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'publicationId':widget.post.sId,
        'userId':user

      }),
    );
    print(response.body);
   var parse = json.decode(response.body);

    this.setState(() {
      if(parse =="1"){
        liked = true ;
      }
      else{
        liked = false;
      }

    });
  }
  getnbLikes() async{

    SharedPreferences prefs1 = await SharedPreferences.getInstance();
    //Return int
    String user = prefs1.getString("id");
    final response = await http.post(
      Uri.http("10.0.2.2:5000", "api/posts/getnbLikes"),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'publicationId':widget.post.sId,


      }),
    );
    print(response.body);
    var parse = json.decode(response.body);

    this.setState(() {
      nbLikes = parse;

    });
  }
  addLike(publicationid) async{

    SharedPreferences prefs1 = await SharedPreferences.getInstance();
    //Return int
    String user = prefs1.getString("id");
    final response = await http.post(
      Uri.http("10.0.2.2:5000", "api/posts/addLike"),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'publicationId':publicationid,
        'userId':user

      }),
    );
    print(response.body);
  }
  dislike(publicationid) async{

    SharedPreferences prefs1 = await SharedPreferences.getInstance();
    //Return int
    String user = prefs1.getString("id");
    final response = await http.post(
      Uri.http("10.0.2.2:5000", "api/posts/dislikePublication"),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'publicationId':publicationid,
        'userId':user

      }),
    );
    print(response.body);
  }
  addComment(publicationId, text) async{
    SharedPreferences prefs1 = await SharedPreferences.getInstance();
    //Return int
    String user = prefs1.getString("id");

    Response response = await http.post(
      Uri.http("10.0.2.2:5000", "api/posts/addComment"),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'publicationId':publicationId,
        'text':text,
        'userId':user,

      }),
    );
    print(response.body);
  }
  deleteComment(publicationId, text) async{
    SharedPreferences prefs1 = await SharedPreferences.getInstance();
    //Return int
    String user = prefs1.getString("id");

    Response response = await http.post(
      Uri.http("10.0.2.2:5000", "api/posts/addComment"),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'publicationId':publicationId,
        'text':text,
        'userId':user,

      }),
    );
    print(response.body);


  }
  showComment(publicationId, text) async{
    SharedPreferences prefs1 = await SharedPreferences.getInstance();
    //Return int
    String user = prefs1.getString("id");

    Response response = await http.post(
      Uri.http("10.0.2.2:5000", "api/posts/showComment"),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'publicationId':publicationId,
        'text':text,
        'userId':user,

      }),
    );
    print(response.body);
  }


}





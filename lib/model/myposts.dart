import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
class MyPosts extends StatefulWidget {

  @override
  _MyPostsState createState() => _MyPostsState();
}



 class _MyPostsState extends State<MyPosts> {
   List data;




   GetMyPost() async{

     SharedPreferences prefs1 = await SharedPreferences.getInstance();
     //Return int
     String user = prefs1.getString("id");
     final response = await http.post(
       Uri.http("10.0.2.2:5000", "api/posts/getPostByUser"),
       headers: <String, String>{
         'Content-Type': 'application/json; charset=UTF-8',
       },
       body: jsonEncode(<String, String>{
         'userId':user

       }),
     );
     print(response.body);

     this.setState(() {
       data = json.decode(response.body);
     });
   }
   deletepost(publicationid) async{


     final response = await http.post(
       Uri.http("10.0.2.2:5000", "api/posts/deletePost"),
       headers: <String, String>{
         'Content-Type': 'application/json; charset=UTF-8',
       },
       body: jsonEncode(<String, String>{
         'publicationId':publicationid

       }),
     );
     print(response.body);
   }
   @override
  void initState() {
    GetMyPost();
    super.initState();
  }
  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(
      title: Text("My Posts"),
      flexibleSpace: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: <Color>[
                  Color(0xd3fa022f),
                  Color(0xffe56464)
                ])
        ),
      ),
      backgroundColor: Colors.red,
    ),
    body:ListView.builder(

  itemCount: data.length,
  itemBuilder: (BuildContext context, int index) {
    return Card(
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(24),
      ),
      child: Column(
        children: [
          Stack(
            children: [
              Ink.image(
                image: NetworkImage(
                  "http://192.168.68.1:5000/uploads/"+data[index]["photo"],
                ),
                height: 240,
                fit: BoxFit.cover,
              ),
              Positioned(
                bottom: 16,
                right: 16,
                left: 16,
                child: Text(
                  data[index]["title"],
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: 24,
                  ),
                ),
              ),
            ],
          ),
          Padding(

            padding: EdgeInsets.only(right: 0,top: 20).copyWith(bottom: 0),
            child: Text(
              data[index]["text"],
              style: TextStyle(fontSize: 16),
            ),
          ),
          ButtonBar(
            alignment: MainAxisAlignment.end,
            children: [
              FlatButton(
                child: Icon(Icons.delete, color: Colors.red),
                //Text('delete this post'),
                textColor: Colors.red,
                onPressed: () {
                  deletepost(data[index]["_id"]);

                },
              ),

            ],
          )
        ],
      ),
    );


  }
    ),
  );









  Widget buildImageInteractionCard() => Card(
    clipBehavior: Clip.antiAlias,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(24),
    ),
    child: Column(
      children: [
        Stack(
          children: [
            Ink.image(
              image: NetworkImage(
                'https://images.unsplash.com/photo-1514888286974-6c03e2ca1dba?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1327&q=80',
              ),
              height: 240,
              fit: BoxFit.cover,
            ),
            Positioned(
              bottom: 16,
              right: 16,
              left: 16,
              child: Text(
                'Cats rule the world!',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  fontSize: 24,
                ),
              ),
            ),
          ],
        ),
        Padding(
          padding: EdgeInsets.all(16).copyWith(bottom: 0),
          child: Text(
            'The cat is the only domesticated species in the family Felidae and is often referred to as the domestic cat to distinguish it from the wild members of the family.',
            style: TextStyle(fontSize: 16),
          ),
        ),
       /* ButtonBar(
          alignment: MainAxisAlignment.start,
          children: [
            FlatButton(
              child: Text('Buy Cat'),
              onPressed: () {},
            ),
            FlatButton(
              child: Text('Buy Cat Food'),
              onPressed: () {},
            )
          ],
        )*/
      ],
    ),
  );
}
import 'package:pim_flutter_app/model/Comments.dart';

class Post {
  String sId;
  String text;
  String title;
  String photo;
  String date;
  List<Comments> comments;

  Post({this.sId, this.text, this.title, this.photo, this.date, this.comments});

  Post.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    text = json['text'];
    title = json['title'];
    photo = json['photo'];
    date = json['date'];
    if (json['comments'] != null) {
      comments = new List<Comments>();
      json['comments'].forEach((v) {
        comments.add(new Comments.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['text'] = this.text;
    data['title'] = this.title;
    data['photo'] = this.photo;
    data['date'] = this.date;
    if (this.comments != null) {
      data['comment'] = this.comments.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

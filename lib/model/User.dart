import 'package:flutter/foundation.dart';

class User {
  final String id;
  final String firstName;
  final String lastName;
  final String phone;
  final String email;
  final String password;
  final String dateOfBirth;
  final String avatar;

  User({
    @required this.id,
    @required this.firstName,
    @required this.lastName,
    @required this.phone,
    @required this.email,
    @required this.password,
    @required this.dateOfBirth,
    @required this.avatar,
  });



  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['_id'] as String,
      firstName: json['firstName'] as String,
      lastName: json['lastName'] as String,
      phone: json['phone'] as String,
      email: json['email'] as String,
      password: json['password'] as String,
      dateOfBirth: json['dateOfBirth'] as String,
      avatar: json['avatar'] as String,
    );
  }
}
class Comments {
  String date;
  String sId;
  String text;
  String author;

  Comments({this.date, this.sId, this.text, this.author});

  Comments.fromJson(Map<String, dynamic> json) {
    date = json['date'];
    sId = json['_id'];
    text = json['text'];
    author = json['author'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this.date;
    data['_id'] = this.sId;
    data['text'] = this.text;
    data['author'] = this.author;
    return data;
  }
}
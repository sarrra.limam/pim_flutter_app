import 'package:flutter/material.dart';
import 'package:pim_flutter_app/auth/SignInScreen.dart';
import 'package:pim_flutter_app/auth/SignUPScreen.dart';
import 'package:pim_flutter_app/auth/ForgetPasswordScreen.dart';
import 'package:pim_flutter_app/auth/HomeScreen.dart';
import 'package:pim_flutter_app/screens/HomeChat.dart';
import 'package:pim_flutter_app/screens/ListComments.dart';
import 'package:pim_flutter_app/screens/newsByCat.dart';
import 'AddNews.dart';
import 'file:///C:/Users/Sarra%20Limam/AndroidStudioProjects/pim_flutter_app/lib/screens/search.dart';
import 'package:pim_flutter_app/screens/editprofilpage.dart';
import 'package:pim_flutter_app/screens/homepage.dart';
import 'package:pim_flutter_app/screens/profilpage.dart';

import 'home.dart';
import 'model/myposts.dart';

void main() =>runApp(MyApp());

/*class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Home(),
    );
  }
}*/




class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Sign Up Screen ',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.teal,
      ),
      initialRoute: 'SignIn',
      routes: {
        'SignIn':(context)=>SignInScreen(),
        'SignUp':(context)=>SignUpScreen(),
        'Home':(context)=>Home(),
        'EditProfilPage':(context)=> EditProfilPage(),
        'search':(context)=> Search(),



        'HomeChat':(context)=> HomeChat(),

        'profilepage':(context)=>ProfilPage(),
        'myposts':(context)=>MyPosts(),
        'ForgetPassword':(context)=>ForgetPassword(),
        'AddNews':(context)=> AddNews(),


      },
    );
  }
}




/*
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Home(),
    );
  }
}*/